#tool nuget:?package=NUnit.ConsoleRunner&version=3.4.0
#addin "Cake.Npm"
#addin "Cake.Figlet"

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

var applicationDirectories = new string[] {
    "../Production/Applications/Windows",
    "../Production/Controller/Tools/Controller/DirectCommand"
};


Task("Restore-Packages")
    .Does(() =>
{
    foreach(string dir in applicationDirectories)
    {
        Information("Restoring packages of {0}",dir);
        var settings = new NpmInstallSettings();

        settings.LogLevel = NpmLogLevel.Info;
        settings.WorkingDirectory = dir;

        NpmInstall(settings);
    }
});

Task("Build")
    .IsDependentOn("Restore-Packages")
    .Does(() =>
{

});

Task("Run-Unit-Tests")
    .IsDependentOn("Build")
    .Does(() =>
{
    foreach(string dir in applicationDirectories)
    {
        Information("Running unit tests of {0}",dir);
        var settings = new NpmRunScriptSettings();

        settings.ScriptName = "test";
        settings.WorkingDirectory = dir;

        NpmRunScript(settings);
    }
});

Task("Default")
    .IsDependentOn("Run-Unit-Tests");

Information(Figlet("Home Automation"));
RunTarget(target);
