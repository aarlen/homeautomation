const argv = require('yargs').argv;

let bindToPort = true;
if(typeof(argv.simulate) !== "undefined") {
    bindToPort = false;
}

let lastCommand = "51731XXX";
let lastType = "0";
let port = null;

exports.initialize = function(server) {
    const serialport = require('serialport');
    const readline = require('readline');

    if(bindToPort) {
        port = new serialport("COM4", {
            baudRate: 9600
        });
        
        const consoleIn = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        
        port.on("open", function () {
            console.log('open');
            const parser = port.pipe(new serialport.parsers.Readline({ delimiter: '\n' }));
            parser.on('data', function(data){
                console.log(`${data}`);
                const codeString = "code=";
                const codeIndex = data.indexOf(codeString);
                if(codeIndex != -1) {
                    lastCommand = data.substring(codeIndex + codeString.length).trim();
                }

                const typeString = "type=";
                const typeIndex = data.indexOf(typeString);
                if(typeIndex != -1) {
                    lastType = data.substring(typeIndex + typeString.length).trim();
                }
               
            });
        });
    }

    const registerUri = function(uri,response) {
        server.get(uri, response);
        server.post(uri, response);
        server.head(uri, response); 
    }

    function getLastCommand(req, res, next) {
        console.log(`returning last type ${lastType} and code ${lastCommand}`);
        res.send({
            "type" : lastType,
            "code" : lastCommand
        });
        next();
    }

    function sendCommand(req,res,next) {
        if(port != null) {
            const command = req.params.command;
            console.log(`sending ${command}`);
            res.send(`sending ${command}`);
            port.write(`${command}\n`);
        } else {
            console.log(`port not bound`);
            res.send(`port not bound`); 
        }
        next();
    }
      
    registerUri('/getLastCommand', getLastCommand);
    registerUri('/sendCommand/:command',sendCommand);
    
    console.log('registered all services');
}