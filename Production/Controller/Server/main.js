const express = require('express');
const path = require('path');
const services = require('./services.js');
var server = express();

server.use(express.static(path.join(__dirname,'/..')));
services.initialize(server);
server.listen(3000, () => console.log('listening on port 3000!'));