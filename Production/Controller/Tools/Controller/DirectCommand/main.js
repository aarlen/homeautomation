const serialport = require('serialport');
const readline = require('readline');


const port = new serialport("COM4", {
    baudRate: 9600
  });

       
  const consoleIn = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

  function SendCommand() {

    consoleIn.question('Enter command and press enter: ', (cmd) => {
        console.log(`Sending ${cmd} to controller`);
        port.write(`${cmd}\n`);
      });
  }

  port.on("open", function () {
    console.log('open');

    const parser = port.pipe(new serialport.parsers.Readline({ delimiter: '\n' }));
    parser.on('data', function(data){
        console.log(`${data}`);

        SendCommand();

    });
  });

