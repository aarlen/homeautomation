#include <IRremote.h>

#define PanasonicAddress      0x4004     // Panasonic address (Pre data) 
#define PanasonicPower        0x100BCBD  // Panasonic Power button
#define RAW 0x406A954D
#define SAMSUNG_VOL   0xE0E0E01F
unsigned int samsungRaw [] = {31969,91,89,11,34,11,33};


IRsend irsend;

int RECV_PIN = 11;
IRrecv irrecv(RECV_PIN);
decode_results decodeResults;

void statusOn() 
{  
  digitalWrite(LED_BUILTIN, HIGH);  
}
void statusOff() 
{  
  digitalWrite(LED_BUILTIN, LOW);  
}

void blinkStatus(bool blinkShort)
{
  statusOn(); 
  delay(blinkShort ? 50 : 1000);                      
  statusOff();  
}

void blinkStatusShort() 
{ 
  blinkStatus(true);
}
void blinkStatusLong() 
{ 
  blinkStatus(false);
}

void setup() {
   Serial.begin(9600);
  irrecv.enableIRIn(); 
   
   pinMode(LED_BUILTIN, OUTPUT);
  /*pinMode(3,OUTPUT);
   digitalWrite(3,HIGH);*/
  blinkStatusShort();
  Serial.write("ready\n");
}

String inString = "";
unsigned long code=0;

unsigned long StrToHex(char str[],int len)
{
  if(len == 8) {
    char lower [] = { str[4],str[5],str[6],str[7],'\0' };
    char higher [] = { str[0],str[1],str[2],str[3],'\0' };
    unsigned long lowVal = strtol(lower, 0, 16);
    unsigned long highVal = strtol(higher, 0, 16);
     return (highVal << 16) + lowVal; 
 
  } else {

    return strtol(str, 0, 16);
  }
}

bool getCommandFromSerial()
{
  if (Serial.available()) {
    statusOn();
    int inChar = Serial.read();

    if (isHexadecimalDigit(inChar)) {
      inString += (char)inChar;
    }
    if (inChar == '\n') {
      code = StrToHex((char*)inString.c_str(),inString.length());
      Serial.print("Value:");
      Serial.println(code,HEX);
      Serial.print("String: ");
      Serial.println(inString);
      statusOff();
      return true;
    }
  }

  if(inString.length() > 8) {
    inString = "";
    Serial.println("overflow");
  }

  statusOff();
  return false;
}

void loop() {
   if(getCommandFromSerial())
   {
      statusOn();
      //irsend.sendPanasonic(PanasonicAddress,PanasonicPower); 
     //irsend.sendSAMSUNG(SAMSUNG_VOL,32);
     irsend.sendSAMSUNG(code,32);
     inString = "";
      statusOff();
   }

         /*statusOn();
      irsend.sendPanasonic(PanasonicAddress,PanasonicPower); 
      delay(1000);
      statusOff();*/

     /* statusOn();
      //irsend.sendSAMSUNG(SAMSUNG_VOL,32); 
      irsend.sendRaw(samsungRaw, sizeof(samsungRaw) / sizeof(samsungRaw[0]), 38); 
      delay(1000);
      statusOff();*/



   if (irrecv.decode(&decodeResults)) {
    statusOn();
    Serial.write("type=");
    Serial.println((int)decodeResults.decode_type,HEX);
    Serial.write("code=");
    Serial.println(decodeResults.value, HEX);
    
    delay(1000);
    statusOff();
    irrecv.resume(); // Receive the next value
  }
}
