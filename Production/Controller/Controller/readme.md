# Controller

Micro-controller to send IR commands to devices.

## Getting Started

These instructions will get you up and running with this project. 

## Prerequisites

* Arduino uno [https://store.arduino.cc/usa/arduino-uno-rev3]
* IRRemote library [https://github.com/z3t0/Arduino-IRremote]

## Installing
* Download IRRemote from github
* Import IRRemote into Arduino IDE

## Contributing

## Versioning


## Authors
* **Andrew Arlen** <andrewkeith80@gmail.com>
* **Samuel Mena**

## License

## Acknowledgements
* IRRemote Team TBD

