const {app, BrowserWindow,ipcMain} = require('electron');
const commands = require("./src/host/commands.js");
const controller = require("./src/host/controller.js");

function createWindow () {
    const win = new BrowserWindow(
        {
            width: 800, 
            height: 600
        });
    win.loadFile('index.html');
}

app.on('ready', createWindow);

function documentReady(event,args){
   commands.initialize(event,ipcMain);
   controller.initialize(event,ipcMain);
}

ipcMain.on('documentReady', documentReady);
