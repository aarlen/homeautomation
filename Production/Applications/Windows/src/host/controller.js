const http = require('http');
const request = require('request');


// Configure the request
var options = {
    url: null,
    method: 'GET',
    "rejectUnauthorized": false,
    json: true,
    headers: {
        'User-Agent': 'Super Agent/0.0.1',
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}

function getLastCommand(event) {
    console.log(`getting last command`);
    http.get("http://localhost:3000/getLastCommand",(resp) => {
        let codeFromController = {};
        resp.on('data',(data) => {
            console.log(`obtained data ${data}`);
            codeFromController = JSON.parse(data);
        });

        resp.on('end',() => {
            console.log(`request complete with ${codeFromController}`);
            
            event.sender.send('codeFromController',codeFromController);
        })
    });
}

function postToService(command, callback) {
    var request = require('request');

    options.url = `http://localhost:3000/sendCommand/${command}`;
    console.log(`sending ${options.url}`);

    request(options, function (error, response, body) {
        let reply = 'unknown';
        if (!error && response.statusCode == 200) {
            reply = body.Result;
        } else {
            reply = `request ${options.url} has failed`;
        }

        if (typeof (callback) !== "undefined") {
            callback(reply);
        }
    });
}

function sendCommand(event,code) {
    console.log(`sending command to controller ${code}`);
    postToService(code);
}

exports.initialize = function(event,ipcMain) {
    ipcMain.on("getLastCommand",getLastCommand);
   ipcMain.on("sendCommand",sendCommand);
}