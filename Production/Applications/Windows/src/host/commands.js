const fs = require('fs');
const commandFileName = 'commands.json';
let data;

function addNewCodeToBrand(codes,newCode) {
    codes.push({
        "label" : newCode.name,
        "code" : newCode.code
    });
}

function addNewBrand(commands,newCode) {
    const newBrand = {
        "brand" : newCode.brand,
        "codes" : []
    };
    addNewCodeToBrand(newBrand.codes,newCode);
    commands.push(newBrand);
}

function save() {
    const dataJson = JSON.stringify(data,null,'\t');
    fs.writeFileSync(commandFileName, dataJson);
}

function saveNewCommand(event,newCode) {
    console.log(`new code ${newCode.name} ${newCode.brand} ${newCode.code}`);
    const brand = data.commands.find(c => c.brand == newCode.brand);
    if(brand) {
        const code = brand.codes.find(c => c.label == newCode.name);
        if(code) {
            code.code = newCode.code;
        } else {
            addNewCodeToBrand(brand.codes,newCode);
        } 
    } else {
        addNewBrand(data.commands,newCode);
    }

    save();
    event.sender.send('initialize',data);
}



exports.getData = function() {
    return data;
}

exports.initialize = function(event,ipcMain) {
    data = JSON.parse(fs.readFileSync(commandFileName, 'utf8'));
   event.sender.send('initialize',data);
   ipcMain.on("saveNewCommand",saveNewCommand);
}