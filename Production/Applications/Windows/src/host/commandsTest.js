const assert = require('assert');
const sinon = require('sinon');
const rewire = require('rewire');
const fs = require('fs');
const commands = rewire('./commands');
const deepEqual = require('deep-equal');
const sampleData = fs.readFileSync("sampleCommands.json", 'utf8');
const fsStub = {
  readFileSync: sinon.fake.returns(sampleData),
  writeFileSync : sinon.fake()
}
commands.__set__('fs', fsStub);

const eventStub = {
  "sender" : {
    "send" : sinon.fake()
  }
}

const ipcMainStub = {
  "on" : sinon.fake()
}

const newCode = {
  name : 'name1',
  brand : 'brand1',
  code : 'code1'
}; 

it("initialize should load data",() =>{

  commands.initialize(eventStub,ipcMainStub); 

  const sampleLoadedData = JSON.parse(sampleData);
  const data = commands.getData();

  deepEqual(sampleLoadedData,data);
  assert(fsStub.readFileSync.calledOnce);
  assert(eventStub.sender.send.calledOnce);
  assert(eventStub.sender.send.calledWith('initialize'));
  assert(ipcMainStub.on.calledOnce);
  assert(ipcMainStub.on.calledWith('saveNewCommand'));

  eventStub.sender.send.resetHistory();
  ipcMainStub.on.resetHistory();
});

it("saveNewCommand should save and initialize",() =>{

  ipcMainStub.on = (event,callback) => 
  { 
    callback(eventStub,newCode);
  };
  commands.initialize(eventStub,ipcMainStub);

  assert(eventStub.sender.send.calledTwice);
  assert(eventStub.sender.send.calledWith('initialize'));
  assert(fsStub.writeFileSync.calledOnce);

  ipcMainStub.on = sinon.fake();
});

it("saveNewCommand with new command should add command",() =>{

  ipcMainStub.on = (event,callback) => 
  { 
    callback(eventStub,newCode);
  };
  commands.initialize(eventStub,ipcMainStub);
  const data = commands.getData();
  const newBrand = data.commands.find(c => c.brand == newCode.brand);
  assert(newBrand);
  ipcMainStub.on = sinon.fake();
});

it("saveNewCommand with new code for existing command should add code",() =>{

  const newCode = {
    name : 'on',
    brand : 'Samsung',
    code : 'code1'
  };

  ipcMainStub.on = (event,callback) => 
  { 
    callback(eventStub,newCode);
  };
  commands.initialize(eventStub,ipcMainStub);
  const data = commands.getData();
  const newBrand = data.commands.find(c => c.brand == newCode.brand);
  const newAddedCode = newBrand.codes.find(c => c.label == newCode.name);
  assert(newAddedCode.code == newCode.code);
  ipcMainStub.on = sinon.fake();
});

it("saveNewCommand with new command for existing brand should add command",() =>{

  const newCode = {
    name : 'power',
    brand : 'Samsung',
    code : 'code1'
  };

  ipcMainStub.on = (event,callback) => 
  { 
    callback(eventStub,newCode);
  };
  commands.initialize(eventStub,ipcMainStub);
  const data = commands.getData();
  const newBrand = data.commands.find(c => c.brand == newCode.brand);
  const newAddedCode = newBrand.codes.find(c => c.label == newCode.name);
  assert(newAddedCode.code == newCode.code);
  ipcMainStub.on = sinon.fake();
});