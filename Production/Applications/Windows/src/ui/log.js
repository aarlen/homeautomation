
const logs = [];

exports.logMsg = function(msg) {
    if(logs.length > 3) {
        logs.splice(0,1);
    }
    logs.push(msg);
    
    $("#console").val(logs.join("\n"));
}