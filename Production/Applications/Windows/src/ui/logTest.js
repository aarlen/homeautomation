const assert = require('assert');
const sinon = require('sinon');
const rewire = require('rewire');
const jsdom = require('jsdom');
const fs = require('fs');

const indexHtml = fs.readFileSync('./index.html', 'utf8');
const { JSDOM } = jsdom;
const { window } = new JSDOM(indexHtml);
global.$ = require('jquery')(window);
const log = require('./log');

it("logMsg should log a message in the logs",() =>{

    log.logMsg("message1");

    const result = $("#console").val();
    assert(result == "message1"); 
}); 