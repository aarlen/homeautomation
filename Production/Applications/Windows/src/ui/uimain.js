const log = require(requireBasePath + "\\log.js");
const send = require(requireBasePath + "\\send.js");
const portConfig = require(requireBasePath + "\\portConfig.js");
const receive = require(requireBasePath + "\\receive.js");

const {ipcRenderer} = require('electron');


 function documentReady(){

    ipcRenderer.on('initialize',(event,args) => {
        const commands = args.commands;
    
        send.initializeCommands(commands);
        portConfig.initialize(args.COMPort);
    
        log.logMsg("initialized");
    
    });

    send.initialize(ipcRenderer);
    receive.initialize();
    ipcRenderer.send("documentReady");
 }

if(typeof(document) !== "undefined") {
    $(document).ready(documentReady);
} 

/*exports.raiseReady = function() {
    documentReady();
}*/

