global.requireBasePath = __dirname;
const assert = require('assert');
const sinon = require('sinon');
const rewire = require('rewire');
const jsdom = require('jsdom');
const fs = require('fs');

const indexHtml = fs.readFileSync('./index.html', 'utf8');
const { JSDOM } = jsdom;
const { window } = new JSDOM(indexHtml);

global.$ = require('jquery')(window);
global.document = window.document;
const uimain = rewire('./uimain.js');

const ipcRendererStub = {
    on : sinon.fake(),
    send: sinon.fake()
}

const sendStub = {
    initialize : sinon.fake(),
    initializeCommands : sinon.fake()
}

const receiveStub = {
    initialize : sinon.fake()
}

const portConfigStub  = { 
    initialize : sinon.fake()
}

const logStub = {
    logMsg : sinon.fake()
}

uimain.__set__('ipcRenderer', ipcRendererStub);
uimain.__set__('log',logStub);
uimain.__set__('send',sendStub);
uimain.__set__('portConfig',portConfigStub);
uimain.__set__('receive',receiveStub);

it("document ready, must initialize modules",() =>{
    sendStub.initialize.resetHistory();
    receiveStub.initialize.resetHistory();
    let initializeCallback;
    ipcRendererStub.on = function(name,callback) {
        initializeCallback = callback;
    }

    uimain.raiseReady();

    initializeCallback('event',{
        commands : "commands",
        COMPort : "comport"
    });

    assert(sendStub.initialize.calledOnce);
    assert(receiveStub.initialize.calledOnce);
    assert(sendStub.initializeCommands.calledWith("commands"));
    assert(portConfigStub.initialize.calledWith('comport'));
    assert(ipcRendererStub.send.calledWith('documentReady'));

}); 