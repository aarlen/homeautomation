const log = require("./log.js");
const {ipcRenderer} = require('electron');

exports.initialize = function() {
    const scannedCodeName = $("#scannedCodeName");
    const scannedBrand = $("#scannedBrand");
    const scannedCode = $("#scannedCode");

    ipcRenderer.on('codeFromController',(event,args) => {
        log.logMsg(`codeFromController ${args}`);
        switch(args.type) {
            case "3":
            scannedBrand.val("NEC");
            break;
            case "5":
            scannedBrand.val("Panasonic");
            break;
            case "7":
                scannedBrand.val("Samsung");
                break;
        }
        scannedCode.val(args.code);
    
    });

    $("#receive").click(() => {
        log.logMsg('receiving from controller');
        ipcRenderer.send("getLastCommand");
    });


    $("#save").click(() => {
        const code = {
            "name" : scannedCodeName.val(),
            "brand" : scannedBrand.val(),
            "code" : scannedCode.val()
        };


        ipcRenderer.send("saveNewCommand",code);
    });
}