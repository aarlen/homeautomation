const assert = require('assert');
const sinon = require('sinon');
const rewire = require('rewire');
const jsdom = require('jsdom');
const fs = require('fs');

const indexHtml = fs.readFileSync('./index.html', 'utf8');
const { JSDOM } = jsdom;
const { window } = new JSDOM(indexHtml);

global.$ = require('jquery')(window);
global.document = window.document;
const receive = rewire('./receive');

const ipcRendererStub = {
    on : sinon.fake(),
    send: sinon.fake()
}
receive.__set__('ipcRenderer', ipcRendererStub);
const save =  $("#save");
const scannedCode = $("#scannedCode");

it("initialize receive",() =>{

    scannedCode.val('100');
    receive.initialize();
    save[0].dispatchEvent(new window.MouseEvent('click'));
    
    assert(ipcRendererStub.send.calledOnce);
}); 