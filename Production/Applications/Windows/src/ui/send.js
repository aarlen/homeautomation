let selectedCommand = null;
exports.initialize = function(ipcRenderer) {
    $("#send").button();
    $(".controlgroup").controlgroup();
    $("#send").click(() => {
        if(selectedCommand != null) {
            const value = selectedCommand.value.split(":=")[1].trim();
            log.logMsg(`sending ${value} to controller`);
            ipcRenderer.send("sendCommand",value);
        }

    });
}

const refreshCommandSelect = function(commands,label) {
    const brand = commands.find(x => x.brand == label);
    const commandOptions = brand.codes.map(x => `<option>${x.label} := ${x.code}</option>`);

    $("#commands option").remove();
    $("#commands").append(commandOptions.join(" ")).selectmenu("refresh").selectmenu({
        change:function(event,ui) {
            log.logMsg(`selected ${ui.item.label}`);
            selectedCommand = ui.item;
        }
    });
 }

const refreshBrandSelect = function(commands) {
    const brandOptions = commands.map(c => `<option>${c.brand}</option>`);
    $("#brand option").remove();
    $("#brand").append(brandOptions.join(" ")).selectmenu("refresh").selectmenu({
            change: function( event, ui ) {
                const label = ui.item.value;
                log.logMsg(`selected ${label}`);

                refreshCommandSelect(commands,label);

            }
          });

          refreshCommandSelect(commands,commands[0].brand);
 }

 exports.initializeCommands = function(commands) {
    refreshBrandSelect(commands);
 }