const assert = require('assert');
const sinon = require('sinon');
const rewire = require('rewire');
const jsdom = require('jsdom');
const fs = require('fs');

const indexHtml = fs.readFileSync('./index.html', 'utf8');
const { JSDOM } = jsdom;
const { window } = new JSDOM(indexHtml);

global.$ = require('jquery')(window);
const portConfig = require('./portConfig');

it("initialize port config, ensure COM port is set",() =>{
    portConfig.initialize(4);

    const result = global.$("#COMPort").val();
    assert(result == 4); 
}); 