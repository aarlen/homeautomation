
function deployment()
{
    const container = $('#deployment')[0];

    mxEvent.disableContextMenu(container);
    
    const graph = new mxGraph(container);

    new mxRubberband(graph);

    const parent = graph.getDefaultParent();

    graph.getModel().beginUpdate();
    try
    {
        
        const v1 = graph.insertVertex(parent, null, 'Cloud VM', 20, 20, 80, 30);
        
        const v2 = graph.insertVertex(parent, null, 'Android / iOS Application', 200, 20, 140, 30);
        graph.insertEdge(parent, null, 'Commands', v1, v2);
        const v3 = graph.insertVertex(parent, null, 'Infrared controller', 450, 20, 140, 30);
        graph.insertEdge(parent, null, 'Commands', v2, v3);


        const v5 = graph.insertVertex(parent, null, 'TV / AV receiver', 330, 180, 120, 30,"rounded;strokeColor=red");
        graph.insertEdge(parent, null, 'infrared Commands', v3, v5,"strokeColor=red");
        graph.insertEdge(parent, null, 'wifi Commands', v2, v5);

        const v6 = graph.insertVertex(parent, null, 'motors and servos', 530, 180, 120, 30,"rounded;strokeColor=red");
        graph.insertEdge(parent, null, 'infrared Commands', v3, v6,"strokeColor=red");
    }
    finally
    {
        graph.getModel().endUpdate();
    }
};

function cloud()
{
    const container = $('#cloud')[0];

    mxEvent.disableContextMenu(container);
    
    const graph = new mxGraph(container);

    new mxRubberband(graph);

    const parent = graph.getDefaultParent();

    graph.getModel().beginUpdate();
    try
    {
        
        const v1 = graph.insertVertex(parent, null, 'Azure storage', 20, 60, 80, 30); 
        const v2 = graph.insertVertex(parent, null, 'Customer Registration', 150, 20, 140, 30);              
        const v3 = graph.insertVertex(parent, null, 'Device subscription', 150, 60, 140, 30);
        const v4 = graph.insertVertex(parent, null, 'Command List', 150, 100, 140, 30);

        graph.insertEdge(parent, null, '', v1, v2);
        graph.insertEdge(parent, null, '', v1, v3);
        graph.insertEdge(parent, null, '', v1, v4);

    }
    finally
    {
        graph.getModel().endUpdate();
    }
};

function mobile()
{
    const container = $('#mobile')[0];

    mxEvent.disableContextMenu(container);
    
    const graph = new mxGraph(container);

    new mxRubberband(graph);

    const parent = graph.getDefaultParent();

    graph.getModel().beginUpdate();
    try
    {
        
        const v1 = graph.insertVertex(parent, null, 'Command Manifest from Cloud', 20, 20, 160, 30); 
        const v2 = graph.insertVertex(parent, null, 'Transmitter', 220, 20, 140, 30);              
        const v3 = graph.insertVertex(parent, null, 'To Controller', 400, 20, 120, 30);

        graph.insertEdge(parent, null, '', v1, v2);
        graph.insertEdge(parent, null, '', v2, v3);

    }
    finally
    {
        graph.getModel().endUpdate();
    }
};

function controller()
{
    const container = $('#controller')[0];

    mxEvent.disableContextMenu(container);
    
    const graph = new mxGraph(container);

    new mxRubberband(graph);

    const parent = graph.getDefaultParent();

    graph.getModel().beginUpdate();
    try
    {
        
        const v1 = graph.insertVertex(parent, null, 'Receiver', 20, 20, 80, 30); 
        const v2 = graph.insertVertex(parent, null, 'infra-red transmitter', 150, 20, 140, 30);              
        const v3 = graph.insertVertex(parent, null, 'TV / AV receiver', 330, 20, 120, 30,"rounded;strokeColor=red");    
        const v4 = graph.insertVertex(parent, null, 'motor / servo', 330, 60, 120, 30,"rounded;strokeColor=red");

        graph.insertEdge(parent, null, '', v1, v2);
        graph.insertEdge(parent, null, '', v2, v3);
        graph.insertEdge(parent, null, '', v2, v4);

    }
    finally
    {
        graph.getModel().endUpdate();
    }
};

$(document).ready(function() {
    if (!mxClient.isBrowserSupported())
    {
        mxUtils.error('Browser is not supported!', 200, false);
    }
    else
    {
        deployment();
        cloud();
        mobile();
        controller();
    }
});