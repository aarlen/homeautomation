# Home Automation

Collection of applications to provide home automation.

## Getting Started

These instructions will get you up and running with this project. 

## Prerequisites

## Build
```
build\localbuildandpackage.bat
```

## Contributing

## Versioning

## Documentation

- Architecture [Documentation/Architecture/Architecture.md]

## Authors
* **Andrew Arlen** <andrewkeith80@gmail.com>
* **Kelvin Kay**
* **Samuel Mena**

## License
Private

## Acknowledgements

